from django.urls import path
from catalog import views


urlpatterns = [
    path('', views.index, name='index'),
]

# add url pattern for displaying books
urlpatterns += [
    path('books/', views.BookListView.as_view(), name='books'),
]

# ad url pattern for displaying book details.
urlpatterns += [
    path('book/<int:pk>', views.BookDetailView.as_view(), name='book-detail'),
]

# add url pattern for displaying Author
urlpatterns += [
    path('authors/', views.AuthorListView.as_view(), name='authors')
]

# add url pattern for the books of the author
urlpatterns += [
    path('author/<int:pk>', views.AuthorDetailView.as_view(), name='author-detail')
]